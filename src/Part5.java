package ua.nure.udaltsov.Practice1;

public class Part5 {

	public static void main(String[] args) {
		
		try{
			
		int number = Integer.parseInt(args[0]);
				
		int sum = 0;
		int y = 10;
		
			while(number > 0){ 
		
			sum += number % y;
			
			number /= y;
			
		}
		
		System.out.println("Part5 => " + sum);
		
		}catch(NumberFormatException e){
				
			System.out.println("You've entered not a number");
		}
		
	}

}