package ua.nure.udaltsov.Practice1;
public class Part7 {

	private static final int ALPHABET_SIZE = 26;
    private static final int CHAR_OFFSET = 64;

    public static void main(String[] args) {

       	
		System.out.println("Part7");

        showTransformationResult(args);
    }
	
	private static void showTransformationResult(String [] numbers) {
		for(int i = 0; i < numbers.length; i++) {
			
			String columnName = numbers[i].toUpperCase();
			
			System.out.println(columnName + "==> " + chars2digits(columnName) + "==> " + digits2char(chars2digits(columnName)));
			
			System.out.println(rightColumn(columnName) + "==> " + chars2digits(rightColumn(columnName)) + "==> " + digits2char(chars2digits(rightColumn(columnName))));
			
		}
		
		
	}	
		

    private static int getNumberOfLetter(int letter) {
		
        return letter - CHAR_OFFSET;
    }
	
	private static String rightColumn(String number) {
		
		int nextColumn = chars2digits(number) + 1;
		
		return digits2char(nextColumn);
		
	}

    private static int chars2digits(String number) {
		
		char[] letters = number.toCharArray();

        int res = getNumberOfLetter(letters[0]);

        int index = 1;

        while (index < letters.length) {

            res = res * ALPHABET_SIZE + getNumberOfLetter(letters[index]);

            index++;
        }
        return res;
    }

    private static String digits2char(int number) {

        StringBuilder letters = new StringBuilder();

        while (number > ALPHABET_SIZE) {

            int left = number % ALPHABET_SIZE;

            if (left == 0) {
                number -= 1;

                letters.append('Z');

            } else {
                number -= left;

                letters.append((char) (left + CHAR_OFFSET));
            }

            number /= ALPHABET_SIZE;
        }

        letters.append((char) (number + CHAR_OFFSET));

        return letters.reverse().toString();
    }

}