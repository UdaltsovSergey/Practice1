package ua.nure.udaltsov.Practice1;

public class Demo {

	public static void main(String[] args) {
		
		Part1.main(new String[] {""});
		Part2.main(new String[] {"5", "10"});
		Part3.main(new String[] {"hello", "my", "dear", "friend"});
		Part4.main(new String[] {"25", "5"});
		Part5.main(new String[] {"3524"});
		Part6.main(new String[] {"10"});
		Part7.main(new String[] {"A", "Z", "AZ", "ZZ"});
		
	}

}