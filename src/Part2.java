package ua.nure.udaltsov.Practice1;

public class Part2 {

	public static void main(String[] args) {
		
		try{
			
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);
		
		int res = x + y;
		
		System.out.println("Part2 => " + res);
		
		}catch(NumberFormatException e){
			System.out.println("You've entered not a number");
		}
		
		
	}

}