package ua.nure.udaltsov.Practice1;

public class Part6 {

	public static void main(String[] args) {
		
		try{
	    int arraySize = Integer.parseInt(args[0]);

		System.out.print("Part6 => ");
		
        for (int el: simpleArray(arraySize) ) {
            System.out.print(el + " ");
        }
		}catch(NumberFormatException e){
			
			System.out.println("You've entered not a number ");
		
		}
		
		System.out.println();
	}
	
	public static int[] simpleArray(int arraySize) {
		
		int[] numbers = new int[arraySize];

        int indexOfElement = 0;

        int number = 2;

        while (indexOfElement < arraySize) {
            boolean flag = true;

            for (int j = 2; j < number; j++) {

                if (number % j == 0) {
                    flag = false;
                }
            }

            if (flag) {
                numbers[indexOfElement] = number;
                indexOfElement++;
            }
            number++;
        }
		return numbers;
	}

}